Name:                ldaptive
Version:             2.1.1
Release:             1
Summary:             LDAP library for Java
License:             ASL 2.0 or LGPLv3
URL:                 http://www.ldaptive.org/
Source0:             https://github.com/vt-middleware/%{name}/archive/v2.1.1/%{name}-%{version}.tar.gz
Source1:             0001-remove-unsupported-dependency.patch
BuildRequires:       maven-local mvn(com.googlecode.json-simple:json-simple)
BuildRequires:       mvn(com.sun.codemodel:codemodel) mvn(commons-cli:commons-cli)
BuildRequires:       mvn(javax.servlet:javax.servlet-api) mvn(net.sf.ehcache:ehcache-core)
BuildRequires:       mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:       mvn(org.apache.maven.plugins:maven-checkstyle-plugin) mvn(org.slf4j:slf4j-api)
BuildRequires:       mvn(org.springframework:spring-beans) mvn(org.springframework:spring-context)
BuildRequires:       mvn(org.springframework:spring-core) mvn(org.springframework:spring-expression)
BuildRequires:       mvn(org.testng:testng)
BuildRequires:       mvn(com.google.code.gson:gson)
BuildRequires:       mvn(org.eclipse.aether:aether-connector-basic)
BuildRequires:       mvn(org.eclipse.aether:aether-transport-wagon)
BuildRequires:       mvn(org.apache.maven.wagon:wagon-http)
BuildRequires:       mvn(org.apache.maven.wagon:wagon-provider-api)
BuildRequires:       java-11-openjdk-devel
Requires:            java-11-openjdk	
Requires:            javapackages-tools
BuildArch:           noarch
%description
Ldaptive is a simple, extensible Java API for interacting with LDAP servers.
It was designed to provide easy LDAP integration for application developers.

%package beans
Summary:             Ldaptive Beans
%description beans
Mapping, persistence, and code generation API for reading and
writing POJOs to an LDAP directory.

%package javadoc
Summary:             Javadoc for ldaptive
%description javadoc
This package contains javadoc for ldaptive.

%package json
Summary:             Ldaptive Json
%description json
Provides JSON reader and writer.

%package parent
Summary:             Ldaptive Parent POM
%description parent
Ldaptive Parent POM.

%package templates
Summary:             Ldaptive Templates
%description templates
Templating functionality for aggregating LDAP searches.

%prep
%setup -q -n ldaptive-%{version}
find . -name "*.class" -print -delete
find . -name "*.jar" -print -delete
%pom_remove_plugin -r :maven-javadoc-plugin
%pom_remove_plugin -r :maven-source-plugin
%pom_remove_plugin -r :maven-enforcer-plugin
%pom_remove_plugin -r :maven-checkstyle-plugin
%pom_remove_plugin -r :japicmp-maven-plugin
%pom_remove_plugin -r :versions-maven-plugin
cp -p distribution/LICENSE* .
cp -p distribution/NOTICE .

%build
export JAVA_HOME=%{_jvmdir}/java-11-openjdk
%mvn_build -sfb
patch -p1 < %{SOURCE1}

%install
%mvn_install

%files -f .mfiles-ldaptive
%doc README.md
%license LICENSE* NOTICE

%files beans -f .mfiles-ldaptive-beans

%files javadoc -f .mfiles-javadoc
%license LICENSE* NOTICE

%files json -f .mfiles-ldaptive-json

%files parent -f .mfiles-ldaptive-parent
%license LICENSE* NOTICE

%files templates -f .mfiles-ldaptive-templates

%changelog
* Tue Jan 16 2024 Ge Wang <wang__ge@126.com> - 2.1.1-1
- Update to version 2.1.1

* Tue Aug 25 2020 Jeffery.Gao <gaojianxing@huawei.com> - 1.1.0-1
- Package init
